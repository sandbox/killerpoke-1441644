DESCRIPTION
--------------------------
Enables users to use the invite plugin to invite friends directly to their 
groups. This is done by hooking into the invite form, providing a selectbox
with organic groups and invite friends directly into this groups.

In the organic groups' global settings, the ability to use this plugin can be
set for each role individually.


CREDITS
----------------------------
- Invite og plugin authored by Franz Wilding -
